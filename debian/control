Source: python-duo-client
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Michael Fladischer <fladi@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 pybuild-plugin-pyproject,
 python3-all,
 python3-freezegun,
 python3-pytest,
 python3-setuptools,
 python3-tz,
Standards-Version: 4.6.2
Homepage: https://github.com/duosecurity/duo_client_python/
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-duo-client
Vcs-Git: https://salsa.debian.org/python-team/packages/python-duo-client.git
Rules-Requires-Root: no
X-Style: black

Package: python3-duo-client
Architecture: all
Depends:
 ${misc:Depends},
 ${python3:Depends},
Description: Interact with the Duo Auth, Admin, and Accounts APIs (Python3 version)
 Python library for interacting with the Duo Auth, Admin, and Accounts APIs. The
 Duo Auth API is a low-level, RESTful API for adding strong two-factor
 authentication to your website or application. The Duo Admin API provides
 programmatic access to the administrative functionality of Duo Security's
 two-factor authentication platform. The Accounts API allows Duo Beyond, Access,
 and MFA customers to create, manage, and delete additional related Duo Security
 customer accounts.
 .
 This package contains the Python 3 version of the library.
